/* eslint no-magic-numbers: 0 */
import React, {Component} from 'react';

import { ReactSelectSearch } from '../lib';

const options = [
    {name: 'Swedish', value: 'sv'},
    {name: 'English', value: 'en'},
    {
        type: 'group',
        name: 'Group name',
        items: [
            {name: 'Spanish', value: 'es'},
        ]
    },
];

class App extends Component {


    constructor() {
        super();
        this.state = {
            id:'options_list',
            value: 'opt3',
            placeholder: 'Yo',
            options: options,
            search:true
        };
        this.setProps = this.setProps.bind(this);
    }

    setProps(newProps) {
        this.setState(newProps);
    }

    render() {
        return (
            <div>
                <ReactSelectSearch
                    setProps={this.setProps}
                    {...this.state}
                />
            </div>
        )
    }
}

export default App;
