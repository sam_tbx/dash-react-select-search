import React, {Component} from 'react';
import PropTypes from 'prop-types';
// import { useSelect } from 'react-select-search';
import SelectSearch from 'react-select-search';

import './reactSelectSearch.css';

// const CustomSelect = ({options, value, multiple, search, disabled}) => {
//     const [snapshot, valueProps, optionProps] = useSelect({
//         options:options,
//         value:value,
//         multiple:multiple,
//         search:search,
//         disabled:disabled,
//     });
//
//     return (
//         <div>
//             <button {...valueProps}>{snapshot.displayValue}</button>
//             {snapshot.focus && (
//                 <ul>
//                     {snapshot.options.map((option) => (
//                         <li key={option.value}>
//                             <button {...optionProps} value={option.value}>{option.name}</button>
//                         </li>
//                     ))}
//                 </ul>
//             )}
//         </div>
//     );
// };

export default class ReactSelectSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
          value: props.value,
        };
    }

    render() {

        const {id, className, setProps, placeholder, value, options, search, multiple, disabled} = this.props;
        console.log(options)

        return (
          <div id={id}>
            <SelectSearch
                placeholder={placeholder}
                options={options}
                multiple={multiple}
                search={search}
                disabled={disabled}
                value={value}
            />
          </div>
        );
    }
}

ReactSelectSearch.defaultProps = {
  multiple: false,
  search: true,
  disabled: false,
  placeholder: null
};

ReactSelectSearch.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    className: PropTypes.string,
    /**
     * The value displayed in the input.
     */
    value: PropTypes.string,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    multiple: PropTypes.bool,
    search: PropTypes.bool,
    disabled: PropTypes.bool

};



export const propTypes = ReactSelectSearch.propTypes;
export const defaultProps = ReactSelectSearch.defaultProps;
