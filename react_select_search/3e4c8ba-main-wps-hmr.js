webpackHotUpdatereact_select_search("main",{

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/react-select-search/style.css":
false,

/***/ "./node_modules/css-loader/dist/cjs.js!./src/lib/components/reactSelectSearch.css":
/*!****************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/lib/components/reactSelectSearch.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "/**\n * Main wrapper\n */\n.select-search {\n    width: 300px;\n    position: relative;\n    font-family: 'Nunito Sans', sans-serif;\n    box-sizing: border-box;\n}\n\n.select-search *,\n.select-search *::after,\n.select-search *::before {\n    box-sizing: inherit;\n}\n\n/**\n * Value wrapper\n */\n.select-search__value {\n    position: relative;\n    z-index: 1;\n}\n\n.select-search__value::after {\n    content: '';\n    display: inline-block;\n    position: absolute;\n    top: calc(50% - 9px);\n    right: 19px;\n    width: 11px;\n    height: 11px;\n}\n\n/**\n * Input\n */\n.select-search__input {\n    display: block;\n    height: 36px;\n    width: 100%;\n    padding: 0 40px 0 16px;\n    background: #fff;\n    border: 1px solid transparent;\n    box-shadow: 0 .0625rem .125rem rgba(0, 0, 0, 0.15);\n    border-radius: 3px;\n    outline: none;\n    font-family: 'Noto Sans', sans-serif;\n    font-size: 14px;\n    text-align: left;\n    text-overflow: ellipsis;\n    line-height: 36px;\n    -webkit-appearance: none;\n}\n\n.select-search__input::-webkit-search-decoration,\n.select-search__input::-webkit-search-cancel-button,\n.select-search__input::-webkit-search-results-button,\n.select-search__input::-webkit-search-results-decoration {\n    -webkit-appearance:none;\n}\n\n.select-search__input:not([readonly]):focus {\n    cursor: initial;\n}\n\n/**\n * Options wrapper\n */\n.select-search__select {\n    background: #fff;\n    box-shadow: 0 .0625rem .125rem rgba(0, 0, 0, 0.15);\n}\n\n/**\n * Options\n */\n.select-search__options {\n    list-style: none;\n}\n\n/**\n * Option row\n */\n.select-search__row:not(:first-child) {\n    border-top: 1px solid #eee;\n}\n\n/**\n * Option\n */\n.select-search__option,\n.select-search__not-found {\n    display: block;\n    height: 36px;\n    width: 100%;\n    padding: 0 16px;\n    background: #fff;\n    border: none;\n    outline: none;\n    font-family: 'Noto Sans', sans-serif;\n    font-size: 14px;\n    text-align: left;\n    cursor: pointer;\n}\n\n.select-search--multiple .select-search__option {\n    height: 48px;\n}\n\n.select-search__option.is-selected {\n    background: #2FCC8B;\n    color: #fff;\n}\n\n.select-search__option.is-highlighted,\n.select-search__option:not(.is-selected):hover {\n    background: rgba(47, 204, 139, 0.1);\n}\n\n.select-search__option.is-highlighted.is-selected,\n.select-search__option.is-selected:hover {\n    background: #2eb378;\n    color: #fff;\n}\n\n/**\n * Group\n */\n.select-search__group-header {\n    font-size: 10px;\n    text-transform: uppercase;\n    background: #eee;\n    padding: 8px 16px;\n}\n\n/**\n * States\n */\n.select-search.is-disabled {\n    opacity: 0.5;\n}\n\n.select-search.is-loading .select-search__value::after {\n    background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'%3E%3Cpath fill='%232F2D37' d='M25,5A20.14,20.14,0,0,1,45,22.88a2.51,2.51,0,0,0,2.49,2.26h0A2.52,2.52,0,0,0,50,22.33a25.14,25.14,0,0,0-50,0,2.52,2.52,0,0,0,2.5,2.81h0A2.51,2.51,0,0,0,5,22.88,20.14,20.14,0,0,1,25,5Z'%3E%3CanimateTransform attributeName='transform' type='rotate' from='0 25 25' to='360 25 25' dur='0.6s' repeatCount='indefinite'/%3E%3C/path%3E%3C/svg%3E\");\n    background-size: 11px;\n}\n\n.select-search:not(.is-disabled) .select-search__input {\n    cursor: pointer;\n}\n\n/**\n * Modifiers\n */\n.select-search--multiple {\n    border-radius: 3px;\n    overflow: hidden;\n}\n\n.select-search:not(.is-loading):not(.select-search--multiple) .select-search__value::after {\n    transform: rotate(45deg);\n    border-right: 1px solid #000;\n    border-bottom: 1px solid #000;\n    pointer-events: none;\n}\n\n.select-search--multiple .select-search__input {\n    cursor: initial;\n}\n\n.select-search--multiple .select-search__input {\n    border-radius: 3px 3px 0 0;\n}\n\n.select-search--multiple:not(.select-search--search) .select-search__input {\n    cursor: default;\n}\n\n.select-search:not(.select-search--multiple) .select-search__input:hover {\n    border-color: #2FCC8B;\n}\n\n\n.select-search:not(.select-search--multiple) .select-search__select {\n    position: absolute;\n    z-index: 2;\n    top: 44px;\n    right: 0;\n    left: 0;\n    border-radius: 3px;\n    overflow: auto;\n    max-height: 360px;\n}\n\n.select-search--multiple .select-search__select {\n    position: relative;\n    overflow: auto;\n    max-height: 260px;\n    border-top: 1px solid #eee;\n    border-radius: 0 0 3px 3px;\n}\n\n.select-search__not-found {\n    height: auto;\n    padding: 16px;\n    text-align: center;\n    color: #888;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/react-select-search/style.css":
false,

/***/ "./src/lib/components/ReactSelectSearch.react.js":
/*!*******************************************************!*\
  !*** ./src/lib/components/ReactSelectSearch.react.js ***!
  \*******************************************************/
/*! exports provided: default, propTypes, defaultProps */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReactSelectSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "propTypes", function() { return propTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultProps", function() { return defaultProps; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_select_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-select-search */ "./node_modules/react-select-search/dist/esm/index.js");
/* harmony import */ var _reactSelectSearch_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reactSelectSearch.css */ "./src/lib/components/reactSelectSearch.css");
/* harmony import */ var _reactSelectSearch_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_reactSelectSearch_css__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


 // import { useSelect } from 'react-select-search';


 // const CustomSelect = ({options, value, multiple, search, disabled}) => {
//     const [snapshot, valueProps, optionProps] = useSelect({
//         options:options,
//         value:value,
//         multiple:multiple,
//         search:search,
//         disabled:disabled,
//     });
//
//     return (
//         <div>
//             <button {...valueProps}>{snapshot.displayValue}</button>
//             {snapshot.focus && (
//                 <ul>
//                     {snapshot.options.map((option) => (
//                         <li key={option.value}>
//                             <button {...optionProps} value={option.value}>{option.name}</button>
//                         </li>
//                     ))}
//                 </ul>
//             )}
//         </div>
//     );
// };

var ReactSelectSearch = /*#__PURE__*/function (_Component) {
  _inherits(ReactSelectSearch, _Component);

  var _super = _createSuper(ReactSelectSearch);

  function ReactSelectSearch(props) {
    var _this;

    _classCallCheck(this, ReactSelectSearch);

    _this = _super.call(this, props);
    _this.state = {
      value: props.value
    };
    return _this;
  }

  _createClass(ReactSelectSearch, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          className = _this$props.className,
          setProps = _this$props.setProps,
          placeholder = _this$props.placeholder,
          value = _this$props.value,
          options = _this$props.options,
          search = _this$props.search,
          multiple = _this$props.multiple,
          disabled = _this$props.disabled;
      console.log(options);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: id
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select_search__WEBPACK_IMPORTED_MODULE_2__["default"], {
        placeholder: placeholder,
        options: options,
        multiple: multiple,
        search: search,
        disabled: disabled,
        value: value
      }));
    }
  }]);

  return ReactSelectSearch;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


ReactSelectSearch.defaultProps = {
  multiple: false,
  search: true,
  disabled: false,
  placeholder: null
};
ReactSelectSearch.propTypes = {
  /**
   * The ID used to identify this component in Dash callbacks.
   */
  id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,

  /**
   * The value displayed in the input.
   */
  value: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,

  /**
   * Dash-assigned callback that should be called to report property changes
   * to Dash, to make them available for callbacks.
   */
  setProps: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  placeholder: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  options: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  multiple: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  search: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
var propTypes = ReactSelectSearch.propTypes;
var defaultProps = ReactSelectSearch.defaultProps;

/***/ }),

/***/ "./src/lib/components/reactSelectSearch.css":
/*!**************************************************!*\
  !*** ./src/lib/components/reactSelectSearch.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./reactSelectSearch.css */ "./node_modules/css-loader/dist/cjs.js!./src/lib/components/reactSelectSearch.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"insertAt":"top","hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../node_modules/css-loader/dist/cjs.js!./reactSelectSearch.css */ "./node_modules/css-loader/dist/cjs.js!./src/lib/components/reactSelectSearch.css", function() {
		var newContent = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./reactSelectSearch.css */ "./node_modules/css-loader/dist/cjs.js!./src/lib/components/reactSelectSearch.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ })

})
//# sourceMappingURL=3e4c8ba-main-wps-hmr.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIzZTRjOGJhLW1haW4td3BzLWhtci5qcyIsInNvdXJjZVJvb3QiOiIifQ==